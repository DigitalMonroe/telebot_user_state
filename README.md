# **Telebot user states**

#### Little library for PyTelegramBotApy and other telegram bot api

##### With the library's functions you can swith user's state in your bot and save user's data

## Documentation

### States functions

#### Class for work with user states init directory .t_bot_users_data states file
UserStates

#### Updates user's states by telegram user's id - _user_id_ and state name _user_state_
`update_state(user_id: int, user_state: str)`


#### Returns get current user's id by _user_id_
`get_current_state(user_id: int)`


#### Returns True or False by _user_id_ and _*states_
`is_current_state(user_id: int, *states)`


### Class UserData for work with users data

#### Creates user's data file by _user_id_

`add_user_in_data(user_id: int)`


#### Returns all user's data by _user_id_

`get_all_data_by_id(user_id: int)`


#### Adds data for _user_id_ with _key_ and key's _data_
`add_data(user_id: int, key, data)`


#### Removes all user's data by _user_id_
`drop_data(user_id: int)`
